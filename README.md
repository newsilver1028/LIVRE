# LIVRE_중고 책 거래 사이트
### 메인 페이지
#### 1. 페이징 기능
![FireShot+Capture+075+-+LIVRE+-+localhost](https://user-images.githubusercontent.com/79626675/116986708-12ffd880-ad09-11eb-8ff1-6d1a7d2eb41d.png)
#### 2. 장르별 페이지
![E689E3E8-0301-46C0-9B7A-6523038504E6_1_105_c](https://user-images.githubusercontent.com/79626675/117003222-8ad8fd80-ad1f-11eb-9482-6ed942418131.jpeg)
#### 3. 검색기능 (제목 / 저자 / 출판사)
![868939E9-B2F4-450D-91F7-43642E84C255_1_105_c](https://user-images.githubusercontent.com/79626675/117007824-09846980-ad25-11eb-8d9a-ccd52bd2b53d.jpeg)
### 제품 상세 페이지
![155E0E5C-EE25-405A-A105-54FE6A18F972](https://user-images.githubusercontent.com/79626675/116994240-6aa34180-ad13-11eb-8554-7db7cd59a888.png)
### 로그인 페이지
![FireShot+Capture+078+-+LIVRE+LOGIN+-+localhost](https://user-images.githubusercontent.com/79626675/116987105-9a4d4c00-ad09-11eb-92c2-1d250858a3bd.png)
### 1. 회원가입
![A01803EE-04C9-4C47-BA63-296D19815356](https://user-images.githubusercontent.com/79626675/116992176-996be880-ad10-11eb-8a28-6cf7a1729a60.png)
#### 🚨 아이디 중복
![635C200C-391C-4238-AA59-C9791BA5EA1F_1_201_a](https://user-images.githubusercontent.com/79626675/117006722-bd84f500-ad23-11eb-8fa1-05a77b856d27.jpeg)
#### 아이디 중복 X
![783785A2-DEE2-43C5-9A0B-1AB3532B4DAB_1_105_c](https://user-images.githubusercontent.com/79626675/117006438-67b04d00-ad23-11eb-9d1b-594bbab69490.jpeg)
#### 생년월일 입력창
![3C679482-4B0D-46DA-BDC7-1A636E6A4B9D_1_105_c](https://user-images.githubusercontent.com/79626675/117003512-e86d4a00-ad1f-11eb-8859-885779dcad15.jpeg)
#### 주소 입력창 (카카오 오픈 API)
![521AADC8-7709-4865-A427-536AE265E877_1_105_c](https://user-images.githubusercontent.com/79626675/117003719-2a968b80-ad20-11eb-9858-092873c505bc.jpeg)
#### 🚨 비밀번호와 비밀번호 확인 일치 X
![1B41CE71-181A-4270-BBE4-2961AA06C917_1_105_c](https://user-images.githubusercontent.com/79626675/117007966-3042a000-ad25-11eb-8119-eee24eb829e9.jpeg)
#### 🚨 null값이 입력되면 정보가 전달되지 않음.
![A42F120B-B7FB-4411-89F4-961C1B042F09_1_105_c](https://user-images.githubusercontent.com/79626675/117008211-726be180-ad25-11eb-8f5e-d6b5c6cf5693.jpeg)
### 2. 아이디 찾기
![188076F1-0F8A-4336-8605-D5BC8C4E0214_1_105_c](https://user-images.githubusercontent.com/79626675/116992466-04b5ba80-ad11-11eb-925b-07d0f5ca3f63.jpeg)
### 3. 비밀번호 찾기
![8A847FB3-CD2F-4AD6-8FED-C23A4660F3B3_1_105_c](https://user-images.githubusercontent.com/79626675/116992388-e354ce80-ad10-11eb-824c-8cd1deff399d.jpeg)
### 로그인 후 메인페이지 (SALE BOOK 버튼이 생김)
#### SALE BOOK 버튼으로 판매 게시물 작성
![569AB32B-CEB1-4EF9-873D-E5B2D8F971EE_1_105_c](https://user-images.githubusercontent.com/79626675/116992912-b81eaf00-ad11-11eb-998e-6e749c6296ea.jpeg)
### 📖 중고 책 판매자
### 게시물 작성 페이지
#### 🚨 null값이 입력되면 정보가 전달되지 않음. (기타사항 제외)
#### 🚨 작성한 판매글은 관리자만 열람 가능
#### 🚨 게시물 상태는 '수취 요청'. '수취 승인'으로 변경될 때까지 대기.
![44E1FF54-4EDD-4D37-BCFD-761C46C419CE](https://user-images.githubusercontent.com/79626675/116993079-ef8d5b80-ad11-11eb-84b8-3726d4a7b51f.png)
### 💵 중고 책 구매자
#### 바로구매
#### 🚨 중고 제품 특성상 제품 당 재고가 1권이므로 구매버튼 클리 후 관리자만 열람 가능
![ABC92A3E-8033-40F2-9EA7-12213727B600_1_105_c](https://user-images.githubusercontent.com/79626675/117005914-c2957480-ad22-11eb-9ac5-538e42d50ed7.jpeg)
### 마이페이지
![23045F57-3B18-4711-AEF2-ABA756D35260_1_105_c](https://user-images.githubusercontent.com/79626675/117004079-9ed12f00-ad20-11eb-8fb3-8f8b68f11ad7.jpeg)
#### 1. 내 프로필 / 내가 쓴 글
![C63EFEAE-A1E8-47EE-86DB-0E6E9B7960FD](https://user-images.githubusercontent.com/79626675/116993605-abe72180-ad12-11eb-99f8-b3e4aaf36eff.png)
#### 내가 쓴 글 (판매내역)
#### 🚨 관리자가 게시물 상태를 '수취 승인'으로 변경하면 관리자에게 택배로 보낸 후 송장번호 입력.
![550B567E-D408-4F2B-917C-9953D4D9D9BC_1_105_c](https://user-images.githubusercontent.com/79626675/117004793-7138b580-ad21-11eb-84e6-091f07101ca9.jpeg)
![23FC13D3-8C66-4AE4-8086-D3F266383C19_1_105_c](https://user-images.githubusercontent.com/79626675/117007142-3e43f100-ad24-11eb-9302-da86c5e8a001.jpeg)
#### 2. 주문내역
#### 🚨 체크박스로 체크 후 취소, 교환, 반품 버튼 클릭하면 데이터 베이스 정보 변경
![55BB68D2-5A90-4D41-8E9A-ABB049788893](https://user-images.githubusercontent.com/79626675/116993692-c91bf000-ad12-11eb-94c7-a36f29a9c043.png)
#### 3. 장바구니
![6D916E51-834C-45D2-B259-E76FA441BBD7](https://user-images.githubusercontent.com/79626675/116993868-fd8fac00-ad12-11eb-91c1-916616833209.png)
### 관리자 페이지
#### 1. 모든 게시물 목록
![112E77F3-5C65-4437-B463-1ED9BFBB06D0_1_105_c](https://user-images.githubusercontent.com/79626675/116993922-0f714f00-ad13-11eb-9206-ef0b08113a0d.jpeg)
#### 2. 교환 / 환불 관리 페이지
![C93B800F-70AE-45B2-812B-5E2430B57302](https://user-images.githubusercontent.com/79626675/116994021-2d3eb400-ad13-11eb-9d96-eed7526698ea.png)
